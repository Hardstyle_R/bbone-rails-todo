class Todo < ActiveRecord::Base
  attr_accessible :completed, :completed_at, :title

  #acts_as_squealer options = {
  #    enabled: true,
  #    only_events: [
  #        :before_create
  #    ],
  #    enabled_methods_activity: false
  #}

  #before_hook :log_method_being_called


  def self.complete id, attributes
    Rails.logger.info('Completing todo...')
    ret  = find id
    ret.update_attributes attributes.merge(completed_at: Time.now)
    ret
  end


end
