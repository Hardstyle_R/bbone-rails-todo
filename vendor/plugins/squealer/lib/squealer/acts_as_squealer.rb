
#
# Gives an ability to simple log all AR events and methods being executed while run
#
# Example:
# in your AR model:
#
# acts_as_squealer options = {
#     enabled: true,
#     only_events: [
#       :before_create
#     ],
#     enabled_methods_activity: false
# }
#
#
module Squealer

  module ActsAsSquealer

    # cattr_accessor
    def self.included(base) # :nodoc:
      #base.logger.debug "included called"
      base.extend(ClassMethods)
      base.send(:include, InstanceMethods)

      #base.after_save.delete_if {
      #    |callback| callback.method == :after_save_call_back
      #}

      # attaching AR callbacks
      base.send(:after_initialize, lambda {
        ActiveRecord::Callbacks::CALLBACKS.each { |x|
          process_callback x
        }
      })


    end

    module ClassMethods

      def acts_as_squealer(options = {})
        cattr_accessor :enabled, :enabled_methods_activity, :only_events
        self.enabled = options.key?(:enabled) ? options[:enabled] : true
        self.enabled_methods_activity = options.key?(:enabled_methods_activity) ? options[:enabled_methods_activity] : false

        self.only_events = options.key?(:only_events) ? options[:only_events] : []


        if (self.enabled)
          logger.debug "So we gonna be logging whats happening with our model.....\n\n"
          if(self.enabled_methods_activity)
            before_hook :log_method_being_called
          end
        end
      end

      ###############

      # this is the DSL method that classes use to add before hooks
      def before_hook(method_name)
        hooks << method_name

        logger.debug 'before_hook triggered...'
      end

      # keeps track of all before hooks
      def hooks
        @hooks ||= []
      end


      # @see https://gist.github.com/ryanlecompte/1283413
      def method_added(method_name)
        # do nothing if the method that was added was an actual hook method, or
        # if it already had hooks added to it
        return if hooks.include?(method_name) || hooked_methods.include?(method_name)
        add_hooks_to(method_name)
      end

      private

      # keeps track of all currently hooked methods
      def hooked_methods
        @hooked_methods ||= []
      end

      def add_hooks_to(method_name)
        # add this method to known hook mappings to avoid infinite
        # recursion when we redefine the method below
        hooked_methods << method_name

        # grab the original method definition
        original_method = instance_method(method_name)

        # re-define the method, but notice how we reference the original
        # method definition
        define_method(method_name) do |*args, &block|
          #logger.debug 'Hey! Some method just been added'
          #logger.debug method_name

          # invoke the hook methods
          self.class.hooks.each do |hook|
            method(hook).call :original_method => {
                :name => original_method.name ? original_method.name : [],
                :params => original_method.parameters ? original_method.parameters : [],
                :full_name => original_method.to_s
            }
          end

          # now invoke the original method
          original_method.bind(self).call(*args, &block)
        end
      end

    end

    module InstanceMethods

      # Processing an event by name, logging what is needed (e.g attributes)
      def process_callback(name)
        if (defined?enabled and enabled)

          # if a list of events has been specified, log only theirs activity
          return unless ( (defined?(only_events) and only_events.any? ) ? (only_events.include?(name)) : true)

          logger.debug 'Processing ' + name.to_s + ' event...'
          logger.debug 'And we have:'
          logger.debug '1. Model attributes: '
          logger.debug self.attributes
        end
      end

      # Processing a single method, logging its params and so on
      def log_method_being_called parameters
        if parameters.has_key? :original_method
          logger.debug ('Method "' + parameters[:original_method][:name].to_s + '" has been called (following params within):') if parameters[:original_method].has_key? :name
          logger.debug parameters[:original_method][:params].to_s if parameters[:original_method].has_key? :params
          logger.debug ('Full name: '+ parameters[:original_method][:full_name].to_s) if parameters[:original_method].has_key? :full_name
        end
        logger.debug 'Object attributes at this point:'
        logger.debug self.attributes
      end




    end


  end



end



