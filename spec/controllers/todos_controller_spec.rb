# spec/controllers/contacts_controller_spec.rb

require 'spec_helper'

require 'pp'


describe TodosController do


  describe 'GET #index' do

    before do
      @todos = FactoryGirl.create_list(:todo, 10)

      @todos.each do |todo|
        todo.should be_valid
      end

      get :index, :format => :json

    end


    it 'response should be OK' do
      response.status.should eq(200)
    end

    it 'response should return the same json objects list' do

      response_result = JSON.parse(response.body)

      #response_result.each do |r|
      #  r.sort
      #end
      #
      #todos_arr = JSON.parse(@todos.to_json)
      #todos_arr.each do |todo|
      #  todo.sort
      #end

      # these should do the same
      # response_result.should =~ JSON.parse(@todos.to_json)
      response_result.should match_array(JSON.parse(@todos.to_json))

    end

  end

  describe 'GET #show' do


    before do

      @todo = FactoryGirl.create(:todo)

      params = Hash.new()
      params['id'] = @todo.id

      #request.accept = "application/json"

      get :show, :id => params['id'], :format => :json

    end

    it 'todo should be valid' do
      @todo.should be_valid
    end

    it 'response should be OK' do
      response.status.should eq(200)
    end

    it 'should retrieve a content-type of json' do
      response.header['Content-Type'].should include 'application/json'
    end


    it 'should return the same json object' do

      response_result = JSON.parse(response.body)

      response_result.should eq JSON.parse(@todo.to_json)

    end

  end



  describe 'POST #create' do

    before do

      @todo = FactoryGirl.build(:todo)


      @request_payload = @todo.attributes

      post :create, :parameters => JSON.generate(@request_payload, quirks_mode: true), :format => :json
    end

    it 'should retrieve status code of 201' do
      response.status.should eq(201)
    end


    it 'should retrieve a content-type of json' do
      response.header['Content-Type'].should include 'application/json'
    end

    it 'should retrieve a single todo' do
      response.body.should include('title')
      response.body.should include('id')
      response.body.should include('completed')
    end

    it 'should not add extraneous attributes' do
      response.body.should_not include('smth_else')
    end

  end

  describe 'PUT #update' do

    before do

      @todo = FactoryGirl.create(:todo)

      @initial_title = @todo.title
      @initial_updated_at = @todo.updated_at
      @new_title = 'Title Changed'

      request_payload = { :title => @new_title }

      put :update, :id => @todo.id, :todo => request_payload, :format => :json

      @todo.reload # http://stackoverflow.com/questions/9223336/how-to-write-an-rspec-test-for-a-simple-put-update

    end

    it 'should retrieve status code of 204' do
      response.status.should eq(204)
    end

    it 'updated attributes should not be as initially' do
      @todo.title.should_not eq(@initial_title)
      @todo.updated_at.should_not eq(@initial_updated_at)
    end

    it 'updated attribute should be the the same' do
      @todo.title.should eq(@new_title)
    end

    it 'updated date should be increased' do
      @todo.updated_at.should > @initial_updated_at
    end


  end


  describe 'DELETE #destroy' do


    before do

      @todo = FactoryGirl.create(:todo)

      params = Hash.new()
      params['id'] = @todo.id

      #request.accept = "application/json"

      delete :destroy, :id => params['id'], :format => :json

    end

    it 'should retrieve status code of 204' do
      response.status.should eq(204)
    end

    it 'should not exists anymore' do

      #lambda {
      #  @todo.reload
      #}.should raise_error(ActiveRecord::RecordNotFound)
      expect {
        @todo.reload
      }.to raise_error(ActiveRecord::RecordNotFound)


    end

  end

end