# spec/factories/todos.rb

#require 'ffaker'


require 'factory_girl'


FactoryGirl.define do

  sequence(:random_ranking) do |n|
    @random_rankings ||= (1..10000).to_a.shuffle
    @random_rankings[n]
  end

  factory :todo do
    title { Faker::Lorem.sentence}
    id { FactoryGirl.generate(:random_ranking) }
    completed [true, false].sample
    completed_at Time.new
    created_at Time.new
    updated_at Time.new

  end

  factory :invalid_todo, :parent => :todo do |f|
    f.title nil
  end

end
